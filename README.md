# Minpex linear regression

Régression linéaire pour prédire le prix d'un bien immobilier

## Installation

```
pip3 install pandas
pip3 install numpy
pip3 install matplotlib

pip3 install setuptools
pip3 install sklearn
```

Or

```
pip3 install -r requirements.txt
```
