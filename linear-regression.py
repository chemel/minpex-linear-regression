import pandas as pd
import numpy as np
import matplotlib as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

# Lecture du CSV
data = pd.read_csv('data.csv')
print(data.head())

# Les données sur lesquels entrainer la machine
x = data.iloc[:,1:-1]
print(x.head())

# Les prix des biens
y = data.iloc[:,-1]
print(y.head())

# On split le tableau en deux, un pour l'entrainement, un pour les tests
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

# Construction du modèle
regress = LinearRegression().fit(x_train, y_train)

# Prédiction
y_prediction = regress.predict(x_test)

# Print du résultat final
compare = pd.DataFrame(x_test)
compare['y_test'] = y_test
compare['y_pred'] = y_prediction

print(compare)